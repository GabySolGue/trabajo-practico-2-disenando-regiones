package Grafo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BFS 
{
	private static ArrayList<Integer> L;
	private static boolean[] marcados;
	
	public static boolean esConexo(Grafo g) //existe camino entre todo para de vertices
	{
		if (g==null)
			throw new IllegalArgumentException("Se intento consultar con un grafo null!" );
		
		if (g.tamano() == 0)
			return true;
		
		return alcanzables(g, 0).size() == g.tamano(); //si la cant de alcanzables es igual a la cant de vertices, es conexo
	}

	static Set<Integer> alcanzables(Grafo g, int origen)
	{
		Set<Integer> ret = new HashSet<Integer>();
		inicializar(g, origen);
		
		while (L.size() >0)
		{
			int i =	L.get(0);
			marcados[i] = true;
			ret.add(i);

			agregarVecinosPendientes(g, i);
			L.remove(0);			
		}
		
		return ret;
	}

	private static void inicializar(Grafo g, int origen) 
	{
		L = new ArrayList<Integer>();	
		L.add(origen);
		marcados = new boolean[g.tamano()];
	}

	private static void agregarVecinosPendientes(Grafo g, int i) 
	{
		for (int vertice : g.vecinos(i))
			if (marcados[vertice] == false && L.contains(vertice) == false)
				L.add(vertice);
	}
}

