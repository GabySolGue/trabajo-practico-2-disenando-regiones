package Grafo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

//Enter
public class Grafo
{
	// Representamos el grafo por su matriz de adyacencia
	private boolean[][] A;
	
	// La cantidad de vertices esta predeterminada desde el constructor
	public Grafo(int vertices)
	{
		A = new boolean[vertices][vertices];
	}
	
	// Agregado de aristas
	public void agregarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		A[i][j] = true;
		A[j][i] = true;
	}
	
	// Eliminacion de aristas
	public void eliminarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		A[i][j] = false;
		A[j][i] = false;
	}

	// Informa si existe la arista especificada
	public boolean existeArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		return A[i][j];
	}

	// Cantidad de vertices
	public int tamano()
	{
		return A.length;
	}
	
	// Vecinos de un vertice
	public Set<Integer> vecinos(int i)
	{
		verificarVertice(i);
		
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0; j < this.tamano(); ++j) if( i != j )
		{
			if( this.existeArista(i,j) )
				ret.add(j);
		}
		
		return ret;		
	}
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= A.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j)
	{
		if( i == j )
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}
	
	//Arbol generador minimo con algoritmo de Prim
	/*	
	*/
	//Creo que necesitamos dos cosas o metodos mas: 
	// 1. Lo que nos dé las longitudes de las aristas
	// 2. Lo que nos traiga la arista mas chica dentro de las disponibles (negras)
	
	public Grafo AGM(Grafo g) 
	{
		Grafo agmT = new Grafo(g.tamano());
		
		LinkedList<Integer> verticesT = new LinkedList<Integer>(); //los vertices "amarillos"
		verticesT.add(0); //cualquier vertice 
//		HashSet aristasT = new HashSet();
		int i = 0;
		
		while (i <= g.tamano()-1)
		{
		// If x de aristaMasChica está en verticesT && y de aristaMasChica no está en verticesT	
			agmT.agregarArista(0, 1);
		/* If !verticesT.contains(x)
			verticesT.add(x);
		  If !verticesT.contains(y)
			verticesT.add(y);	
		*/
		}
		return agmT;

	}
}
