package Grafo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

//Enter
public class GrafoConPeso
{
	// Representamos el grafo por su matriz de adyacencia
	private double[][] A;
	
	// La cantidad de vertices esta predeterminada desde el constructor
	public GrafoConPeso(int vertices)
	{
		A = new double[vertices][vertices];
	}
	
	// Agregado de aristas
	public void agregarArista(int i, int j, double peso)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		if (peso <= 0){
			throw new IllegalArgumentException("El peso de la arista no debe ser 0 o negativo: " + peso);
		}
		
		A[i][j] = peso;
		A[j][i] = peso;
	}
	
	// Eliminacion de aristas
	public void eliminarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		A[i][j] = 0;
		A[j][i] = 0;
	}

	// Informa si existe la arista especificada
	public boolean existeArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		
		if (A[i][j] > 0)
			return true;
		
		return false;
	}

	// Cantidad de vertices
	public int tamano()
	{
		return A.length;
	}
	
	// Vecinos de un vertice
	public Set<Integer> vecinos(int i)
	{
		verificarVertice(i);
		
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0; j < this.tamano(); ++j) if( i != j )
		{
			if( this.existeArista(i,j) )
				ret.add(j);
		}
		
		return ret;		
	}
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= A.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j)
	{
		if( i == j )
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}
	
	@Override
	public String toString()
	{
		String s = "";
		
		for(int i=0; i<this.tamano(); ++i)
		{
			for(int j=0; j<this.tamano() ; ++j)
				s += A[i][j] + "  ";
			s += "\n \n";
		}			
		
		return s;
	}
	
	//Arbol generador minimo con algoritmo de Prim
		
	public GrafoConPeso AGM() 
	{
		GrafoConPeso agmT = new GrafoConPeso(this.tamano());
		
	// https://www.youtube.com/watch?v=4fZqs1wamQY
		
		return agmT;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(A);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrafoConPeso other = (GrafoConPeso) obj;
		if (!Arrays.deepEquals(A, other.A))
			return false;
		return true;
	}

	public double[][] getA() {
		return A;
	}

	public void setA(double[][] a) {
		A = a;
	}
	
}

