package Grafo;

import org.junit.Test;
import static org.junit.Assert.*;

import junit.framework.TestCase;

public class TestGrafoConPeso {
	
	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaPesoCero()
	{
		GrafoConPeso g = new GrafoConPeso(3);
		g.agregarArista(0,1, 0);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaPesoNegativo()
	{
		GrafoConPeso g = new GrafoConPeso(3);
		g.agregarArista(0,1, -1);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaNegativa()
	{
		GrafoConPeso g = new GrafoConPeso(3);
		g.agregarArista(-1,1, 0);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaMayor()
	{
		GrafoConPeso g = new GrafoConPeso(3);
		g.agregarArista(3,1, 0);
	}
	
	@Test 
	public void agregarArista()
	{
		GrafoConPeso grafo_esperado = new GrafoConPeso(3);
		double[][] g = new double[3][3];
		g[0][1] = 5;
		g[1][0] = 5;
		grafo_esperado.setA(g);
		
		GrafoConPeso grafo_resultante = new GrafoConPeso(3);
		
		grafo_resultante.agregarArista(0,1, 5);
		
		assertEquals(grafo_esperado, grafo_resultante);
	}
	
	@Test 
	public void eliminarArista()
	{
		GrafoConPeso grafo_esperado = new GrafoConPeso(3);
		double[][] g = new double[3][3];
		g[0][1] = 5;
		g[1][0] = 5;
		grafo_esperado.setA(g);
		
		GrafoConPeso grafo_resultante = new GrafoConPeso(3);
		grafo_resultante.agregarArista(0,1, 5);
		grafo_resultante.agregarArista(2, 0, 1.5);
		
		grafo_resultante.eliminarArista(2, 0);
		
		assertEquals(grafo_esperado, grafo_resultante);
	}
	@Test  (expected=IllegalArgumentException.class)
	public void eliminarAristaIncorrecta()
	{		
		GrafoConPeso grafo_resultante = new GrafoConPeso(3);
		grafo_resultante.agregarArista(0,1, 5);
		grafo_resultante.agregarArista(2, 0, 1.5);
		
		grafo_resultante.eliminarArista(3, 0);
	}
	@Test 
	public void existeAristaInvertida()
	{
		GrafoConPeso g = new GrafoConPeso(3);
		g.agregarArista(0,1, 5);
		
		assertTrue(g.existeArista(1,0));
	}
	
	@Test 
	public void existeAristaEliminada()
	{
		GrafoConPeso g = new GrafoConPeso(3);
		g.agregarArista(0,1, 5);
		g.eliminarArista(1, 0);
		
		assertFalse(g.existeArista(1,0));
	}
}
