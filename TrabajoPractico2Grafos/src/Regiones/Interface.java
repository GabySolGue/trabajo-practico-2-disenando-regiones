package Regiones;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import Grafo.Grafo;
import Grafo.GrafoConPeso;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;

public class Interface {

	private JFrame frame;
	private JTable tabla;
	private JTextField textRegiones;
	private JMapViewer mapa;
	private GrafoConPeso argentina;
	private GrafoConPeso agmArgentina;
	HashMap<String,Coordinate> coordenadasProvincias;

	private JButton btnSimilaridad;
	private JScrollPane scrollPane;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface window = new Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 717, 548);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Regionalizaci�n de Argentina");
		
		textRegiones = new JTextField();
		textRegiones.setBounds(234, 434, 67, 20);
		frame.getContentPane().add(textRegiones);
		textRegiones.setColumns(10);
		
		JLabel lblRegiones = new JLabel("Ingrese la cantidad de regiones:");
		lblRegiones.setBounds(23, 436, 222, 17);
		frame.getContentPane().add(lblRegiones);
		
		
		
		JButton btnRegionalizar = new JButton("Regionalizar");
		btnRegionalizar.setBounds(112, 464, 108, 23);
		frame.getContentPane().add(btnRegionalizar);
		
		btnSimilaridad = new JButton("Cargar Similaridad");
		btnSimilaridad.setBounds(91, 400, 154, 23);
		frame.getContentPane().add(btnSimilaridad);
		
		//Creo un Map con las provincias y sus respectivas coordenadas (Ciudades capitales)
		//Lo uso para tomar los datos y dibujar los marcadores en el mapa
		//�Sirve realmente? No se! ya estoy mareada
		coordenadasProvincias = new HashMap<String,Coordinate>();
		coordenadasProvincias.put("CABA", new Coordinate(-34.61315, -58.37723));
		coordenadasProvincias.put("Buenos Aires", new Coordinate(-34.92145, -57.95453));
		coordenadasProvincias.put("Cordoba", new Coordinate(-31.4135, -64.18105));
		coordenadasProvincias.put("Misiones", new Coordinate(-27.36708, -55.89608));
		coordenadasProvincias.put("Tucuman", new Coordinate(-26.82414, -65.2226));
		coordenadasProvincias.put("Mendoza", new Coordinate(-32.89084, -68.82717));
		coordenadasProvincias.put("San Juan", new Coordinate(-31.5375, -68.53639));
		coordenadasProvincias.put("Salta", new Coordinate(-24.7859, -65.41166));
		coordenadasProvincias.put("Santa F�", new Coordinate(-31.64881, -60.70868));
		coordenadasProvincias.put("Chaco", new Coordinate(-27.46056, -58.98389));
		coordenadasProvincias.put("Santiago del Estero", new Coordinate(-27.79511, -64.26149));
		coordenadasProvincias.put("Jujuy", new Coordinate(-24.19457, -65.29712));
		
		mapa = new JMapViewer();
	//	mapa.setZoomControlsVisible(true);
		mapa.setLayout(null);
		mapa.setBounds(-33, 5, 400, 400); //no entiendo porqu� no se puede "estirar" -.-
		Coordinate coordenada = new Coordinate(-37.97257835059323, -66.41678013059582);//Nos posicionamos en Argentina
		mapa.setDisplayPosition(coordenada, 4);
		
		JPanel panelMapa = new JPanel(); //JPanel para poner el mapa dentro
		panelMapa.setBounds(339, 11, 333, 487);
		frame.getContentPane().add(panelMapa);
		panelMapa.setLayout(null);
		panelMapa.add(mapa);
		
		
		
		//La tabla debe tener todas las combinaciones entre provincias vecinas y el usuario ingresar�a los indices de similaridad	
		
		tabla = new JTable();
		tabla.setColumnSelectionAllowed(true);
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Provincia 1");
		model.addColumn("Provincia 2");
		model.addColumn("Similaridad");
		model.addRow(new String[] { "CABA", "Buenos Aires","" });
		model.addRow(new String[] { "Buenos Aires","Entre Rios", "" });
		model.addRow(new String[] { "Buenos Aires","La Pampa",""});
	//  **** Seguir agregando*****
		tabla.setModel(model);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(23, 75, 299, 314);
		frame.getContentPane().add(scrollPane);
		scrollPane.setViewportView(tabla);
		
		JLabel lbl_indicacion1 = new JLabel("Complete los indices de Similiaridad en  \r\n la tabla, a continuaci\u00F3n pulse 'Cargar Similaridad'");
		lbl_indicacion1.setBounds(23, 33, 299, 32);
		frame.getContentPane().add(lbl_indicacion1);
		
		//La idea es agregar un marcador por cada provincia
		MapMarker mkrBue = new MapMarkerDot("Buenos Aires", coordenadasProvincias.get("Buenos Aires"));
		mkrBue.getStyle().setBackColor(Color.black);
		mkrBue.getStyle().setColor(Color.yellow);
		mapa.addMapMarker(mkrBue);
		
		MapMarker mkrCABA = new MapMarkerDot("CABA", coordenadasProvincias.get("CABA"));
		mkrCABA.getStyle().setBackColor(Color.black);
		mkrCABA.getStyle().setColor(Color.yellow);
		mapa.addMapMarker(mkrCABA);
		
		MapMarker mkrCor = new MapMarkerDot("Cordoba", coordenadasProvincias.get("Cordoba"));
		mkrCor.getStyle().setBackColor(Color.black);
		mkrCor.getStyle().setColor(Color.yellow);
		mapa.addMapMarker(mkrCor);
		
		MapMarker mkrMis = new MapMarkerDot("Misiones", coordenadasProvincias.get("Misiones"));
		mkrMis.getStyle().setBackColor(Color.black);
		mkrMis.getStyle().setColor(Color.yellow);
		mapa.addMapMarker(mkrMis);
		
		MapMarker mkrStaFe = new MapMarkerDot("Santa F�", coordenadasProvincias.get("Santa F�"));
		mkrStaFe.getStyle().setBackColor(Color.black);
		mkrStaFe.getStyle().setColor(Color.yellow);
		mapa.addMapMarker(mkrStaFe);
	//  ****Seguir agregando*****
		
		//Inicializo grafos con tantos vertices como provincias hay
		argentina = new GrafoConPeso(27);
		agmArgentina = new GrafoConPeso(27);
		
		//Cargar similaridad significa agregar las aristas con un peso determinado 
		btnSimilaridad.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				//Aca tengo que poder leer los datos de la tabla.
				//Hay que tener un sistema para codificar a cada provincia(String) con un numero que ser� el v�rtice
				//  para tener la combinacion i,j y el peso de su arista
				//Llamar a argentina.agregarArista (i,j,peso)
				
				agmArgentina = argentina.AGM();//una vez agregadas las aristas, genero el AGM
				dibujarArbol(); //mostramos en el mapa el arbol generado
			}
		});
		
		btnRegionalizar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String str = textRegiones.getText();
				int cant_regiones = Integer.parseInt(str);
				
				//buscar tantas aristas "costosas" en agmArgentina como cant_regiones-1 quiera el usuario
				
				//eliminar dichas aristas de agmArgentina
				
				dibujarArbol(); //mostramos el arbol modificado, es decir, las regiones
			}
		});
		
	}
	
	private void dibujarArbol() 
	{
		//generar las rectas de las aristas y mostrarlo en el mapa
	}
}
